from datetime import datetime, timedelta
from sqlalchemy import *
from json import dumps, loads
import os

meta = MetaData()
eng = create_engine(os.environ['DATABASE_URL'])
meta.bind = eng
conn = eng.connect()
create_all = meta.create_all
drop_all = meta.drop_all
def reset():
    drop_all()
    create_all()
create_all()

users = Table(
    'users', meta,
    Column('id', Integer, primary_key=True),
    Column('name', Unicode(100)),
    Column('email', Unicode(255), unique=True, nullable=True),
    Column('password', Unicode(128), nullable=True),
    Column('fb_id', Unicode(512), nullable=True),
    Column('fb_at', Unicode(512), nullable=True),
    Column('wp_id', Unicode(512), nullable=True),
    Column('wp_at', Unicode(512), nullable=True),
    Column('ava_url', Unicode(640), nullable=True),
    Column('location_id', Integer, nullable=True),
    Column('confirmed', Integer(1), default=0, nullable=False),
    Column('created', DateTime, default=datetime.now())
)

cities = Table(
    'cities', meta,
    Column('id', Integer, primary_key=True),
    Column('fb_id', Unicode(255)),
    Column('name', Unicode(1026))
)

settings = Table(
    'settings', meta,
    Column('id', Integer, primary_key=True),
    Column('newsletter', Integer(1), default=1, nullable=False),
    Column('friend_actions', Integer(1), default=1, nullable=False)
)

class User(object):

    def __init__(self, password=None, email=None, location=None, name=None,
                 fb_id=None, fb_at=None, wp_id=None, wp_at=None):
        self.email = email
        self.password = password
        self.location = City.create(location)
        self.name = name
        self.fb_id = fb_id
        self.fb_at = fb_at
        self.wp_id = wp_id
        self.wp_at = wp_at

    @staticmethod
    def create(e, p, l, n):
        users.insert().execute(email=e, password=p, location=City.create(l), name=n)

    @staticmethod
    def fb_create(fb_id, fb_at, e, l, n):
        users.insert().execute(fb_id=fb_id, fb_at=fb_at, email=e, location=City.fb_create(l), name=n)

    @staticmethod
    def update(e, l, n, fb_id, fb_at):
        conn.execute(users.update().where(users.c.email==e).values(name=n , fb_id=fb_id, fb_at=fb_at,
                     location=City.fb_create(l)))

    @staticmethod
    def wp_update(id, wp_id, wp_at):
        conn.execute(users.update().where(users.c.id==id).values(wp_id=wp_id, wp_at=wp_at))

    @staticmethod
    def from_e(e):
        return users.select(users.c.email==e).execute().fetchone()

    @staticmethod
    def from_uid(uid):
        return users.select(users.c.id==uid).execute().fetchone()

    @staticmethod
    def from_fb_id(fbid):
        return users.select(users.c.fb_id==fbid).execute().fetchone()

    @staticmethod
    def from_fb_at(fbat):
        return users.select(users.c.fb_at==fbat).execute().fetchone()

    @staticmethod
    def from_wp_id(fbid):
        return users.select(users.c.fb_id==fbid).execute().fetchone()

    @staticmethod
    def from_wp_at(fbat):
        return users.select(users.c.fb_at==fbat).execute().fetchone()

    def __repr__(self):
        return 'User %d: "%s"' % (self.name, self.email)

class City(object):

    def __init__(self, fb_id=None, name=None):
        self.name = name
        self.fb_id = fb_id

    @staticmethod
    def from_fb_id(fb_id):
        return cities.select(cities.c.fb_id==fb_id).execute().fetchone()

    @staticmethod
    def from_name(name):
        return cities.select(cities.c.name==name).execute().fetchone()

    @staticmethod
    def create(l):
        name = l['name']
        if not City.from_name(name):
            cities.insert().execute(name=name)
        return cities.select(cities.c.name==name).execute().fetchone()['id']

    @staticmethod
    def fb_create(l):
        name = l['name']
        fb_id = l('id')
        if City.from_name(name) and not City.from_fb_id(fb_id):
            conn.execute(cities.update().where(cities.c.name==name).values(fb_id=fb_id))
        elif City.from_fb_id(fb_id) and not City.from_name(name):
            conn.execute(cities.update().where(cities.c.fb_id==fb_id).values(name=name))
        elif not City.from_fb_id(id) and not City.from_name(name):
            cities.insert().execute(name=name, fb_id=fb_id)
        return cities.select(cities.c.fb_id==fb_id).execute().fetchone()['id']

    @staticmethod
    def from_e(e):
        return cities.select(cities.c.email==e).execute().fetchone()

class Settings(object):

    def __init__(self, newsletter=1, friend_actions=1):
        self.newsletter = newsletter
        self.friend_action = friend_action

    @staticmethod
    def update_password(id, np):
        pass

    @staticmethod
    def update_personal_info(id, name, email, location):
        pass

    @staticmethod
    def update_email_settings(newsletter, friend_actions):
        pass