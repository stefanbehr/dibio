import simplejson as json
import sqlite3 as lite
from contextlib import closing

from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from wepay import WePay

app = Flask(__name__)
app.config.from_envvar('DIBIO_SETTINGS', silent=True)

local = 'http://127.0.0.1:5000'

def connect_db():
    return lite.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as schema:
            db.cursor().executescript(schema.read())
        db.commit()

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/authorize')
def authorize():
    wepay = WePay(app.config['IN_PRODUCTION'])
    # redirect to wepay for authorization
    return redirect(wepay.get_authorization_url(local + url_for('get_token'), app.config['CLIENT_ID']))

@app.route('/gettoken')
def get_token():
    code = request.args.get('code')
    wepay = WePay(app.config['IN_PRODUCTION'])
    wepay.get_token(local + url_for('get_token'), app.config['CLIENT_ID'],
               app.config['CLIENT_SECRET'], code)
    # at this point, wepay.access_token has been set to the user's token
    return render_template('show_code.html', code=wepay.access_token)

@app.route('/showtoken')
def show_token():
    return "Hello, world"

if __name__ == '__main__':
    app.run()
