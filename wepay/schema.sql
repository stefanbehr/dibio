drop table if exists users;
create table users (
       id integer primary key,
       first_name string not null,
       last_name string not null,
       email string not null,
       registration string not null,
       access_token string not null
);